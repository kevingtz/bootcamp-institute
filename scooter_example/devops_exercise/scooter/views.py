from django.contrib.gis.db.models.functions import Distance
import json
from django.http import JsonResponse
from django.contrib.gis.measure import D
from django.views import View
from scooter.models import ScooterModel

from scooter.utils import create_point


class Scooter(View):
    name = 'API'

    @classmethod
    def _scooter_as_json(cls, scooter):
        scooter_json = {
            'id': scooter.id,
            'code': scooter.code,
            'lat': scooter.latitude,
            'lng': scooter.longitude
        }

        if hasattr(scooter, 'distance'):
            scooter_json['distance'] = scooter.distance.m

        return scooter_json

    def get(self, request, *args, **kwargs):
        """Return the nearest 200 scooters sorted by distance.

        Only consider scooters that are 500 meters or closer.
        """
        MAX_DISTANCE_METERS = 500
        MAX_SCOOTERS_TO_RETURN = 200

        lat = float(request.GET.get('lat'))
        lng = float(request.GET.get('lng'))

        if not lat or not lng:
            raise Exception('Missing parameter')

        location = create_point(lng, lat)

        scooter_query = (
            ScooterModel
                .objects
                .filter(
                    location__distance_lte=(location, D(m=MAX_DISTANCE_METERS))
                )
                .annotate(distance=Distance('location', location))
                .order_by('distance')
        )[:MAX_SCOOTERS_TO_RETURN]

        scooters_as_json = [self._scooter_as_json(scooter) for scooter in scooter_query]

        return JsonResponse({
            'scooters': scooters_as_json
        })

    def post(self, request, *args, **kwargs):
        """Create/Update a scooter."""
        data = json.loads(request.body)

        lat = float(data.get('lat'))
        lng = float(data.get('lng'))
        code = data.get('code')

        if not lat or not lng or not code:
            raise Exception('Missing parameter')

        location = create_point(lng, lat)

        try:
            scooter = ScooterModel.objects.get(code=code)
            scooter.location = location
            scooter.save()
        except ScooterModel.DoesNotExist:
            scooter = ScooterModel.objects.create(
                location=location,
                code=code
            )

        return JsonResponse(self._scooter_as_json(scooter))
