# Setup
* Build: `docker-compose build`
* Start database: `docker-compose up -d db`
* Connect to db and create it:
    * `docker exec -it devops_exercise_db_1 bash`
    * `psql -U postgres`
    * `CREATE DATABASE grin;`
* Run migrations: `docker-compose run web python manage.py migrate`  

# Develop
Run: `docker-compose up`

## Misc

Create migrations: `docker-compose run web python manage.py makemigrations`

Run migrations: `docker-compose run web python manage.py migrate`

Sample GET: `curl --header "Content-Type: application/json" --request GET 'http://localhost:8000/scooter/?lat=19.438057&lng=-99.204968'`

Sample POST: `curl --header "Content-Type: application/json" --request POST  --data '{"code":"ABCD","lat":"19.3910038","lng":"-99.2836966"}'   http://localhost:8000/scooter/`